
n.n.n / 2017-07-21
==================

  * Build site Wed Jul 19 07:58:15 EDT 2017
  * Build site Sun Jul 16 07:08:21 EDT 2017
  * Build site Sat Jul  8 10:18:14 EDT 2017
  * Increase line-height of content
  * Build site Fri Jul  7 08:56:15 EDT 2017
  * Build site Fri Jul  7 08:53:29 EDT 2017
  * Add Stream menu and meta for twitter
  * Build site Thu Jul  6 09:49:57 EDT 2017
  * Build site Wed Jul  5 20:32:37 EDT 2017
  * Build site Wed Jul  5 20:24:56 EDT 2017
  * Build site Wed Jul  5 18:41:18 EDT 2017
  * Build site Wed Jul  5 15:14:01 EDT 2017
  * Add CSS for new caption style and update now.md
  * Build site Tue Jul  4 13:08:08 EDT 2017
  * Add book
  * Add post
  * Build site Mon Jul  3 09:29:05 EDT 2017
  * Build site Fri Jun 30 13:22:45 EDT 2017
  * New post
  * Add intro to micro posts and make sure <strong> text is bold
  * Remove whitespace
  * Don't include micro posts on home page
  * Build site Wed Jun 28 20:38:25 EDT 2017
  * Merge branch 'master' of gitlab.com:jackbaty/batydotnet-hugo
  * New post
  * New post
  * Update post
  * Build site Tue Jun 27 07:58:16 EDT 2017
  * Revert width change
  * Reduce article width
  * Build site Sun Jun 25 14:24:23 EDT 2017
  * Build site Sun Jun 25 13:12:02 EDT 2017
  * Improve site title
  * Add more microformats
  * Add microformats
  * Add minos theme
  * Remove unused theme
  * Build site Sun Jun 25 11:24:51 EDT 2017
  * Build site Sun Jun 25 11:06:57 EDT 2017
  * New theme, config, etc
  * Delete theme
  * Ignore unused themes
  * Build site Sat Jun 24 08:00:06 EDT 2017
  * Build site Wed Jun 21 19:36:24 EDT 2017
  * Build site Wed Jun 21 19:32:46 EDT 2017
  * Build site Wed Jun 21 19:15:44 EDT 2017
  * Build site Wed Jun 21 19:08:24 EDT 2017
  * Build site Wed Jun 21 18:45:23 EDT 2017
  * Build site Wed Jun 21 18:15:31 EDT 2017
  * Build site Tue Jun 20 18:12:06 EDT 2017
  * Remove build step from deploy while using Netlify
  * Build site Tue Jun 20 17:52:31 EDT 2017
  * Build site Tue Jun 20 17:48:44 EDT 2017
  * Added build hook to trigger Netlify
  * Build site Tue Jun 20 14:56:27 EDT 2017
  * Add netlify config file
  * Fix name clash with josie posts
  * Build site Tue Jun 20 07:53:33 EDT 2017
  * Build site Mon Jun 19 20:37:42 EDT 2017
  * Build site Mon Jun 19 20:27:50 EDT 2017
  * Build site Mon Jun 19 18:34:50 EDT 2017
  * Build site Mon Jun 19 09:07:10 EDT 2017
  * Build site Sun Jun 18 20:18:08 EDT 2017
  * Build site Sun Jun 18 20:13:26 EDT 2017
  * Build site Sun Jun 18 08:27:47 EDT 2017
  * Build site Sun Jun 18 08:27:19 EDT 2017
  * Build site Sat Jun 17 14:01:48 EDT 2017
  * Build site Sat Jun 17 14:00:39 EDT 2017
  * Remove tags from display on microblog posts
  * Build site Sat Jun 17 12:03:27 EDT 2017
  * Build site Sat Jun 17 10:07:07 EDT 2017
  * Build site Sat Jun 17 09:18:32 EDT 2017
  * Build site Fri Jun 16 09:49:55 EDT 2017
  * Build site Thu Jun 15 17:30:29 EDT 2017
  * Tweak RSS template
  * Add my own RSS template so .Content is used
  * Add webmention.io links
  * Add gitlab-ci config
  * Fix link to spacemacs.org
  * Build site Thu Jun 15 08:29:50 EDT 2017
  * Build site Thu Jun 15 08:24:20 EDT 2017
  * Build site Thu Jun 15 08:23:49 EDT 2017
  * Build site Thu Jun 15 08:21:53 EDT 2017
  * Build site Tue Jun 13 18:38:02 EDT 2017
  * Build site Tue Jun 13 17:45:54 EDT 2017
  * Build site Tue Jun 13 17:44:28 EDT 2017
  * Build site Tue Jun 13 17:20:51 EDT 2017
  * Build site Tue Jun 13 08:04:23 EDT 2017
  * Build site Tue Jun 13 06:58:24 EDT 2017
  * Build site Tue Jun 13 06:54:43 EDT 2017
  * Build site Mon Jun 12 22:00:25 EDT 2017
  * Build site Sun Jun 11 17:28:36 EDT 2017
  * Build site Sun Jun 11 17:26:23 EDT 2017
  * Build site Sun Jun 11 17:26:00 EDT 2017
  * Build site Sun Jun 11 09:40:36 EDT 2017
  * Build site Sun Jun 11 08:56:43 EDT 2017
  * Removed google analytics
  * Build site Tue Jun  6 17:00:53 EDT 2017
  * Build site Tue Jun  6 13:52:24 EDT 2017
  * Add search form to home page
  * Build site Mon Jun  5 17:42:58 EDT 2017
  * Build site Sun Jun  4 22:11:33 EDT 2017
  * Build site Sun Jun  4 21:45:34 EDT 2017
  * Update now, books, about
  * Update menus
  * Build site Sun Jun  4 16:51:17 EDT 2017
  * Build site Sun Jun  4 16:38:08 EDT 2017
  * Change fonts
  * Build site Sun Jun  4 14:07:29 EDT 2017
  * Build site Sun Jun  4 14:05:51 EDT 2017
  * Build site Sun Jun  4 13:28:02 EDT 2017
  * Build site Sun Jun  4 11:14:07 EDT 2017
  * Build site Sun Jun  4 11:13:09 EDT 2017
  * Build site Fri Jun  2 15:33:57 EDT 2017
  * Build site Sat May 27 12:17:43 EDT 2017
  * Build site Sat May 27 12:17:10 EDT 2017
  * Increase cache header for css
  * Build site Sat May 27 12:05:29 EDT 2017
  * Build site Sun May  7 09:51:41 EDT 2017
  * Remove menu items
  * Remove Mint script
  * Build site Sat May  6 16:28:52 EDT 2017
  * Build site Sat May  6 07:11:58 EDT 2017
  * Build site Fri May  5 16:14:39 EDT 2017
  * Build site Fri May  5 08:46:50 EDT 2017
  * Build site Thu May  4 17:41:58 EDT 2017
  * Build site Thu May  4 17:09:46 EDT 2017
  * Add ADR
  * Add e-content microformat
  * Build site Wed May  3 19:54:57 EDT 2017
  * Build site Wed May  3 19:47:14 EDT 2017
  * Build site Wed May  3 17:51:11 EDT 2017
  * Build site Wed May  3 17:39:51 EDT 2017
  * Build site Wed May  3 17:23:52 EDT 2017
  * Build site Tue May  2 13:50:03 EDT 2017
  * Stop redirecting to www
  * Build site Tue May  2 13:41:35 EDT 2017
  * Fix rel="me" for micro.blog
  * Shorten navigation text
  * Add snippets to side menu
  * Update Now page
  * Edit Micro.blog post
  * Post about Micro.blog
  * Add micro.blog verification to head
  * Add Micro.blog to sidebar
  * Digital photography post
  * Amend Mastodon post
  * Update mastodon post
  * Edit Mastodon
  * Add Mastodon post
  * Josie
  * Edit about page
  * New Post
  * Tweak copy
  * New post
  * Ignore bbedit project
  * Add redirect
  * Edit post
  * New Post
  * Add post
  * Tweak copy a bit more
  * Copy tweak
  * New post
  * Build site Mon Mar 20 14:48:27 EDT 2017
  * Build site Mon Mar 20 14:45:08 EDT 2017
  * Build site Sun Mar 19 13:59:40 EDT 2017
  * Remove unused themes
  * Fix damian conway post formatting
  * Redirect apex non-SSL requests
  * Build site Fri Mar 17 12:21:18 EDT 2017
  * Build site Fri Mar 17 12:18:42 EDT 2017
  * Add redirect for baty.net
  * Build site Fri Mar 17 10:51:13 EDT 2017
  * Updated Blog “2017-03-17-a-test-post-using-netlify-cms”
  * Build site Fri Mar 17 10:38:13 EDT 2017
  * Updated Blog “2017-03-17-a-test-post-using-netlify-cms”
  * Created Blog “2017-03-17-a-test-post-using-netlify-cms”
  * Build site Fri Mar 17 10:00:41 EDT 2017
  * Build site Fri Mar 17 09:54:49 EDT 2017
  * Finally fix gitignore
  * Try again to remove bbedit project
  * Add Netlify CMS
  * Add BBEdit project
  * Build site Tue Mar 14 19:21:30 EDT 2017
  * Build site Tue Mar 14 17:31:54 EDT 2017
  * Change deployment to Netlify
  * Build site Tue Mar 14 12:50:35 EDT 2017
  * Build site Tue Mar 14 12:20:22 EDT 2017
  * Build site Sun Mar 12 16:17:35 EDT 2017
  * Build site Sun Mar 12 16:15:42 EDT 2017
  * Build site Sun Mar 12 15:55:09 EDT 2017
  * Build site Sun Mar 12 13:42:23 EDT 2017
  * Build site Sun Mar 12 13:05:34 EDT 2017
  * Build site Sat Mar 11 11:30:06 EST 2017
  * Build site Sat Mar 11 10:31:41 EST 2017
  * Build site Sat Mar 11 09:53:26 EST 2017
  * Build site Thu Mar  9 08:14:54 EST 2017
  * Update Stuff I Use with new camera gear
  * Build site Wed Mar  8 11:44:08 EST 2017
  * Build site Wed Mar  8 11:42:48 EST 2017
  * Build site Tue Mar  7 20:59:20 EST 2017
  * Build site Tue Mar  7 20:26:42 EST 2017
  * Build site Sun Mar  5 12:19:47 EST 2017
  * Build site Sat Mar  4 12:53:57 EST 2017
  * Build site Sat Mar  4 12:51:33 EST 2017
  * Build site Sat Mar  4 12:43:32 EST 2017
  * Build site Sat Mar  4 08:17:18 EST 2017
  * Build site Wed Mar  1 21:50:16 EST 2017
  * Build site Wed Mar  1 21:10:31 EST 2017
  * Build site Wed Mar  1 21:00:59 EST 2017
  * Build site Sat Dec  3 10:29:27 EST 2016
  * Build site Sat Dec  3 10:03:06 EST 2016
  * Dial the font size down a notch.
  * Include www in base URL
  * Build site Tue Nov 29 19:17:52 EST 2016
  * Update Makefile to use Netlify
  * Build site Mon Nov 28 22:19:09 EST 2016
  * Build site Mon Nov 28 22:18:33 EST 2016
  * Build site Mon Nov 28 22:10:08 EST 2016
  * Build site Sun Nov 27 12:19:52 EST 2016
  * Build site Sat Nov 26 16:44:46 EST 2016
  * Build site Sat Nov 26 16:42:52 EST 2016
  * Build site Sat Nov 26 14:14:40 EST 2016
  * Build site Sat Nov 26 14:04:00 EST 2016
  * Build site Sat Nov 26 13:47:41 EST 2016
  * Build site Sat Nov 26 13:19:10 EST 2016
  * Build site Sat Nov 26 09:26:21 EST 2016
  * Build site Sat Nov 26 08:24:11 EST 2016
  * Build site Sat Nov 26 08:15:05 EST 2016
  * Build site Thu Nov 24 20:37:53 EST 2016
  * Build site Thu Nov 24 20:33:01 EST 2016
  * Build site Thu Nov 24 20:26:46 EST 2016
  * Build site Thu Nov 24 19:59:32 EST 2016
  * Add favicon to new theme
  * Remove purecss and prev-next links
  * Build site Mon Nov 21 20:06:09 EST 2016
  * Build site Mon Nov 21 20:05:19 EST 2016
  * Build site Mon Nov 21 19:14:42 EST 2016
  * Change theme to Lanyon
  * Add Notes link to sidebar
  * Build site Sun Nov 20 13:50:09 EST 2016
  * Add caption via <figure> to shortcode
  * Build site Sat Nov 19 16:46:07 EST 2016
  * Build site Thu Nov 17 10:18:36 EST 2016
  * Remove spotify embed to test performance ratings
  * Build site Wed Nov 16 22:50:43 EST 2016
  * No longer canonify URLs
  * Add subtitle to home page title
  * Update cache headers
  * Add cache headers
  * Build site Wed Nov 16 20:52:52 EST 2016
  * Build site Tue Nov 15 18:26:19 EST 2016
  * Build site Tue Nov 15 09:27:00 EST 2016
  * Build site Tue Nov 15 09:22:08 EST 2016
  * Build site Sun Nov 13 19:36:27 EST 2016
  * Build site Sat Nov 12 16:31:37 EST 2016
  * Build site Wed Nov  9 14:16:24 EST 2016
  * Build site Wed Nov  9 06:31:25 EST 2016
  * Build site Mon Nov  7 17:04:30 EST 2016
  * Build site Mon Nov  7 16:45:47 EST 2016
  * Build site Sun Nov  6 08:35:21 EST 2016
  * Build site Sun Nov  6 08:03:23 EST 2016
  * Build site Sun Nov  6 08:01:42 EST 2016
  * Build site Sat Nov  5 14:50:30 EDT 2016
  * Build site Sat Nov  5 14:47:50 EDT 2016
  * Build site Sat Nov  5 13:13:15 EDT 2016
  * Build site Sat Nov  5 13:01:05 EDT 2016
  * Build site Sat Nov  5 12:51:43 EDT 2016
  * Build site Sat Nov  5 12:32:03 EDT 2016
  * Merge branch 'master' of gitlab.com:jackbaty/baty.net-hugo
  * Convert to YAML
  * Build site Thu Nov  3 16:35:18 EDT 2016
  * Build site Thu Nov  3 16:13:45 EDT 2016
  * Build site Thu Nov  3 15:04:04 EDT 2016
  * Build site Thu Nov  3 14:57:23 EDT 2016
  * Build site Thu Nov  3 07:44:08 EDT 2016
  * Build site Thu Nov  3 07:25:52 EDT 2016
  * Build site Thu Nov  3 07:02:07 EDT 2016
  * Build site Wed Nov  2 20:04:02 EDT 2016
  * Build site Wed Nov  2 19:50:38 EDT 2016
  * Build site Wed Nov  2 19:27:06 EDT 2016
  * Build site Tue Nov  1 06:38:54 EDT 2016
  * Build site Sun Oct 30 18:20:55 EDT 2016
  * Build site Sat Oct 29 09:03:59 EDT 2016
  * Build site Sat Oct 29 08:04:43 EDT 2016
  * Build site Sun Oct 23 15:01:18 EDT 2016
  * Build site Sun Oct 23 14:54:49 EDT 2016
  * Build site Sun Oct 23 14:44:47 EDT 2016
  * Build site Sun Oct 23 14:36:52 EDT 2016
  * New post
  * Add aliases to Pakon post
  * Reduce image size
  * Add photo to Zim post
  * Fix Zim post
  * New post
  * New Post
  * Copy tweaks
  * New post
  * New post
  * Copy change
  * New post
  * New post
  * New post
  * Add underline to article links
  * New post
  * New post
  * Copy edits
  * New post
  * Fixes #2: Clean up post formatting
  * Delete post
  * Copy tweak
  * Copy change
  * Add image to Leeloo post
  * New Post - Leeloo
  * Added static galleries and Leeloo
  * New post
  * Update 404 page
  * Flesh out 404 template
  * Use normal HTML on 404 page
  * Add content to 404
  * Add _redirects
  * Stuff I use updates
  * Update post
  * Update from Forestry.io - 2016/2016/forestryio.md
  * Update from Forestry.io - 2016/2016/forestryio.md
  * Update from Forestry.io - 2016/2016/forestryio.md
  * Update from Forestry.io - 2016/2016/forestryio.md
  * Change delimeter between tags to pipe
  * New post
  * Build site Mon Sep  5 14:12:22 EDT 2016
  * Edit post
  * New post
  * Add Google Analytics and move Mint to bottom
  * Remove default google analytics code
  * Remove Disqus comments
  * Change base of site to www.baty.net for Netlify CDN
  * No longer ignore themes/* since it's not a submodule now
  * add themes directory to repo
  * New post
  * Build site Sat Sep  3 08:46:35 EDT 2016
  * Build site Thu Sep  1 07:04:46 EDT 2016
  * Build site Thu Sep  1 07:03:02 EDT 2016
  * Build site Wed Aug 31 17:01:06 EDT 2016
  * Build site Sun Aug 28 10:01:47 EDT 2016
  * Build site Sun Aug 28 08:08:01 EDT 2016
  * Build site Sun Aug 28 08:06:32 EDT 2016
  * Build site Sat Aug 27 16:34:57 EDT 2016
  * Build site Sat Aug 27 16:33:13 EDT 2016
  * Fix broken posts
  * Add missing posts from prior blog
  * Build site Sun Aug 21 11:08:35 EDT 2016
  * Build site Sun Aug 21 09:49:58 EDT 2016
  * Build site Sun Aug 21 09:47:32 EDT 2016
  * Remove extra file
  * Build site Sun Aug 21 09:44:03 EDT 2016
  * Build site Wed Aug 17 18:27:46 EDT 2016
  * Build site Wed Aug 17 18:26:14 EDT 2016
  * Build site Wed Aug 17 18:21:17 EDT 2016
  * Build site Wed Aug 17 16:31:52 EDT 2016
  * Build site Wed Aug 17 15:13:23 EDT 2016
  * Build site Wed Aug 17 14:58:25 EDT 2016
  * Build site Wed Aug 17 14:52:29 EDT 2016
  * Add .htaccess
  * Build site Wed Aug 17 13:46:25 EDT 2016
  * Build site Wed Aug 17 13:12:53 EDT 2016
  * Removed bad character
  * Fix relative links to images
  * Remove layout: post from header
  * Cleaned up post
  * Clean up a few posts. Add booklist/
  * Replace &#8221; with double quote
  * Replace &#8220; with double quote
  * Replace &#8217; with single quote
  * Cleaned about and funky post
  * Update about page
  * Remove host name from image URLs with http
  * Remove host name from image URLs
  * Fix absolute URLs in link posts
  * Move content from wordpress
  * Pre merge with baty.net
  * Build site Tue Aug 16 14:35:51 EDT 2016
  * Build site Tue Aug 16 14:33:14 EDT 2016
  * Build site Sun Aug 14 13:46:30 EDT 2016
  * Build site Sun Aug 14 13:45:18 EDT 2016
  * Build site Sun Aug 14 13:40:27 EDT 2016
  * Build site Sun Aug 14 13:39:57 EDT 2016
  * Build site Sat Aug 13 11:32:15 EDT 2016
  * Build site Sat Aug 13 11:27:29 EDT 2016
  * Build site Sat Aug 13 11:26:09 EDT 2016
  * Build site Sat Aug 13 09:30:02 EDT 2016
  * Build site Tue Aug  9 19:40:02 EDT 2016
  * Build site Tue Aug  9 19:39:24 EDT 2016
  * Build site Tue Aug  9 17:53:33 EDT 2016
  * Build site Tue Aug  9 17:51:46 EDT 2016
  * Add gitlab post
  * Add new file
  * Build site Sun Aug  7 20:17:02 EDT 2016
  * Add post
  * Build site Wed Jul 13 17:57:48 EDT 2016
  * Build site Mon Jul  4 11:12:34 EDT 2016
  * Build site Mon Jul  4 11:04:53 EDT 2016
  * Build site Mon Jul  4 10:44:46 EDT 2016
  * Build site Mon Jul  4 10:23:55 EDT 2016
  * Build site Mon Jul  4 09:59:28 EDT 2016
  * Build site Mon Jul  4 09:57:36 EDT 2016
  * Ignore temporary files
  * Build site Mon Jul  4 09:54:29 EDT 2016
  * Build site Sun Jul  3 11:17:50 EDT 2016
  * Build site Sun Jul  3 09:38:23 EDT 2016
  * Build site Sat Jul  2 09:34:40 EDT 2016
  * Build site Fri Jul  1 15:07:49 EDT 2016
  * Move post into 2016
  * Add content/
  * Remove post file
  * Update config
  * Ignore /public/
  * Add Makefile
  * First commit
