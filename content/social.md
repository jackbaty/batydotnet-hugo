---
date: 2016-03-12T20:22:40+00:00
title: Follow me elsewhere
type: page
---

Here's where I can be found on social media.

## <i class="fa fa-twitter" aria-hidden="true"></i> Twitter

I mostly just cross-post my [microblog](/micro) posts so it might be redundant. Feel free to @ or DM me though.

<a href="https://twitter.com/jackbaty/" rel="me">@jackbaty</a> on Twitter

## <i class="fa fa-flickr" aria-hidden="true"></i> Flickr

I've been posting photos to Flickr since forever. It's still the best photo sharing service, 
even though it's fallen out of favor in recent years.

<a href="https://flickr.com/photos/jbaty" rel="me">My Flickr</a>

## <i class="fa fa-instagram" aria-hidden="true"></i> Instagram

I only reluctantly post photos on Instagram. I don't like that they only allow posting using their own app. The API is limited and exclusionary. Still, it's where everyone
lives these days.

<a href="https://instagram.com/mrjackbaty" rel="me">mrjackbaty</a> on Instagram

## <i class="fa fa-list-alt" aria-hidden="true"></i> Micro.blog

Micro.blog is "a timeline and publishing platform for the open web"
created by [Manton Reece](http://www.manton.org/). It's a great idea. [Micro.blog](https://micro.blog). I pull my other social feeds into it.

<a href="https://micro.blog/jack" rel="me">@jack</a> on Micro.blog

