---
title: "Micro Posts"
---

**This page contains short ("microblog") posts. They will also show up
on [micro.blog/jack](https://micro.blog/jack) and are cross-posted to [Twitter](https://twitter.com/jackbaty).**

