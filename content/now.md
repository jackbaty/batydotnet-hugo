---
author: Jack
date: 2016-03-12T20:22:40+00:00
title: Things I’m doing now
type: page
---

A few of the things I'm working on or thinking about as of **August 10, 2017**

  * Training [Josie](https://instagram.com/gooddogjosie)
  * Experimenting with an iPad-centric workflow (but it isn't sticking)
  * Messing around with my blogs (I have several now, sigh)
  * Reading [The Rise and Fall of D.O.D.O.](https://www.goodreads.com/book/show/32075825-the-rise-and-fall-of-d-o-d-o) by Neal Stephenson
  * Listening to [John Adams](https://www.goodreads.com/book/show/2203.John_Adams) (audio book) by David McCullough

(_The idea for this page was inspired by [Derek Sivers][3]_)

 [1]: http://fusionary.com
 [3]: https://sivers.org/nowff
