---
date: 2017-12-22T02:09:15+00:00
title: About Jack Baty and Baty.net
---

<img id="your-host" style="width:250px;" src="/img/jack-bw-250.jpg" alt="Jack Baty" width="250" height="250" />

**Hello there. I'm Jack Baty and this used to be my personal space.**

I'm now actively blogging at [jack.baty.net](https://jack.baty.net/)
