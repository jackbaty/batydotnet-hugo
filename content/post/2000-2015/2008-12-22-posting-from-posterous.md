---
author: Jack
categories:
- Journal
date: 2008-12-23T00:11:11+00:00
title: Posting from Posterous
url: /2008/posting-from-posterous/
---

Can Posterous (<http://posterous.com/)> really auto-post to my blog? Let's see&#8230; 

<img src="http://posterous.com/getfile/files.posterous.com/jackbaty/1jS842mvi7pzQhwd3MyqQDI31ZNGiaZf4OH2FQoVnUDepR3rfpK6ihlasZi3/posterous-small.png" width="70" height="55" />

<p style="font-size: 10px;">
  <a href="http://posterous.com">Posted via email</a> from <a href="http://jackbaty.posterous.com/posting-from-posterous-3">Jack Baty's posterous</a>
</p>

**Well there's your answer**