---
author: Jack
categories:
- Journal
date: 2012-12-08T21:54:52+00:00
tags:
- film
- printing
title: The Origin of my Avatar
url: /2012/201212where-my-avatar-came-from/
---

While chatting with a man from whom I was buying some old camera gear, I noticed a pile of negatives on his kitchen table. He said that he'd bought them years before at a garage sale. Didn't know who they belonged to. My nostalgia meter redlined so I asked if they were for sale. They were, so I bought them.

The negatives were simple family photos from the early 1900s. One of my favorites was of a man with a pipe wearing overalls pushing a woman in a wheelbarrow. I love the photo. They both are laughing, and the man's laugh looks so genuine and comical that I now use it for my avatar. It makes me happy.

![Photo](/wp-content/uploads/2012/12/EpsonScan08501-768x542.jpg)

