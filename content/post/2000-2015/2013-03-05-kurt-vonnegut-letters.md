---
author: Jack
categories:
- Books
- Journal
date: 2013-03-05T05:00:00+00:00
tags:
- book
title: 'Book: Kurt Vonnegut – Letters'
url: /2013/kurt-vonnegut-letters/
---

<div>
  <div>
    <div>
      <div>
        <div>
          <div>
            <div>
              <a href="http://www.amazon.com/Kurt-Vonnegut-Letters/dp/0385343752%3FSubscriptionId%3D0ENGV10E9K9QDNSJ5C82%26tag%3Djackbaty-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3D0385343752" target="new"><br /> <img src="http://ecx.images-amazon.com/images/I/519whs6ihzL.jpg" /><br /> </a>
            </div>
            
            <div>
              <a href="http://www.amazon.com/Kurt-Vonnegut-Letters/dp/0385343752%3FSubscriptionId%3D0ENGV10E9K9QDNSJ5C82%26tag%3Djackbaty-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3D0385343752" target="new" class="product-title title">Kurt Vonnegut: Letters</a></p> 
              
              <div>
                By Kurt Vonnegut
              </div>
              
              <p>
                <a href="http://www.amazon.com/Kurt-Vonnegut-Letters/dp/0385343752%3FSubscriptionId%3D0ENGV10E9K9QDNSJ5C82%26tag%3Djackbaty-20%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3D0385343752" target="new" class="buy-button"><input type="button" class="sqs-amazon-button sqs-system-button sqs-editable-button" value="Buy on Amazon" /></a>
              </p>
            </div></p>
          </div>
        </div>
      </div>
    </div>
    
    <div>
      <div>
        <div>
          I have never read any of Vonneguts books, but after reading "Letters" that will have to change. I had no idea that the guy was funny; very funny. His letters made me laugh, but also moved and inspired me.</p> 
          
          <p>
             
          </p>
        </div>
      </div>
    </div>
  </div>
</div>