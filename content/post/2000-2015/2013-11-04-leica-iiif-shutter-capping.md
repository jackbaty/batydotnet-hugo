---
author: Jack
categories:
- Journal
date: 2013-11-04T00:00:00+00:00
tags:
- photography
title: Leica IIIf shutter capping
url: /2013/leica-iiif-shutter-capping/
---

<div>
  <div>
    <div>
      <div>
        <div>
          <p>
            I bought a little Leica IIIf on a whim a year ago and have become rather attached to it. It's tiny, beautiful, and I like how it handles.  
          </p>
        </div>
      </div>
    </div>
  </div>
  
  <div>
    <div>
      <div>
        <div>
          <div>
            <div>
              <div>
                <img alt="Leica IIIf" src="/img/imported/20131104_DSCF0005-edit.jpg" />
              </div>
              
              <div>
                <div>
                  <p>
                    Leica IIIf
                  </p>
                </div>
              </div></p>
            </div></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div>
    <div>
      <div>
        <div>
          <p>
            I went for a walk with it this morning and after developing the roll I discovered that its shutter has begun "capping", which means the second curtain isn't behaving correctly. You can see the results in the examples below. Notice how the right side of the images are darker than the left. 
          </p>
        </div>
      </div>
    </div>
  </div>
  
  <div>
    <div>
      <div>
        <div>
          <div>
            <div>
              <div>
                <p>
                  <img alt="2013_Roll-038_07A.jpg" src="/img/imported/2013_Roll-038_07A.jpg" />
                </p>
                
                <div>
                </div></p>
              </div>
              
              <div>
                <p>
                  <img alt="2013_Roll-038_18.jpg" src="/img/imported/2013_Roll-038_18.jpg" />
                </p>
                
                <div>
                </div></p>
              </div>
              
              <div>
                <p>
                  <img alt="2013_Roll-038_23A.jpg" src="/img/imported/2013_Roll-038_23A.jpg" />
                </p>
                
                <div>
                </div></p>
              </div></p>
            </div>
            
            <div>
              <div>
                <a href="#" class="previous"></a><br /> <a href="#" class="next"></a>
              </div></p>
            </div>
            
            <p>
              <!-- END .sqs-gallery-meta-container -->
            </p>
          </div>
          
          <div>
            <p>
              <img src="/img/imported/2013_Roll-038_07A.jpg" alt="2013_Roll-038_07A.jpg" />
            </p>
            
            <p>
              <img src="/img/imported/2013_Roll-038_18.jpg" alt="2013_Roll-038_18.jpg" />
            </p>
            
            <p>
              <img src="/img/imported/2013_Roll-038_23A.jpg" alt="2013_Roll-038_23A.jpg" />
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div>
    <div>
      <div>
        <div>
          <p>
            Normally, I would just send the camera out for a nice CLA and keep it alive. The problem this time is that I want a "real" IIIf. You see, mine is actually a IIIc converted to a IIIf. That's not a big deal but this one isn't quite as "tight" as it should be. It's also been re-covered and I don't love the way the replacement covering feels. I may just decide to replace the whole thing instead. Dilemma. 
          </p>
        </div>
      </div>
    </div>
  </div>
</div>