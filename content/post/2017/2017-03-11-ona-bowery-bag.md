---
title: "ONA Bowery Bag"
slug: "ona-bowery-bag"
date: "2017-03-11T10:25:52-05:00"
tags: ["Hardware","Photography"]
---

{{< figure src="/img/2017/ona-bowery.jpg" title="" >}}

[The Bowery](https://www.onabags.com/store/messenger-bags/the-bowery.html?color=field-tan)
is my second bag from ONA. I wanted something small that I could carry
everywhere and the Bowery fits the bill. I carry a Leica M, the Fuji
X-Pro2, a couple rolls of film, spare battery, and a notebook. I like
everything about it. I'm told it looks a lot like a purse but that's
fine with me.
