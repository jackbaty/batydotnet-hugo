# 2. Push to Netlify

Date: 2017-08-25

## Status

Accepted

## Context

While I prefer the simple html-files-in-a-folder approach, I do want the CDN,
free SSL, and git-push-do-deploy process of Netlify.

## Decision

I actually did this a month or so ago, but need an ADR to record it.

## Consequences

My site has a few additional moving parts as part of Netlify but I get a number
of benefits that justify them.
