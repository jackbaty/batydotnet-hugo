# 1. Move back to hosted VPS at DigitalOcean

Date: 04/05/2017

## Status

Accepted

## Context

The site has been running via Netlify for a few months. I would prefer fewer
layers between me and my files so the plan is to move back to statically hosted
files on my DigitalOcean box

## Decision

Copy fresh files to server and update Makefile to use rsync instead

## Consequences

- No more automatic CDN
- Can now process web logs with goaccess
- Uptime is all my responsibility

